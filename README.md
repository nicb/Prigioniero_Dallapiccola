# Una introduzione a *Il Prigioniero* di Luigi Dallapiccola

Speech (in italian) delivered:

1. during the SMC-Chigiana online seminars 20/05/2022

## LICENSE

<img
  src=http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png
  width=88
  alt="CreativeCommons Attribution-ShareAlike 4.0"
/>
