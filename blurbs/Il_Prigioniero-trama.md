# *Il Prigioniero*

## Musica

Luigi Dallapiccola

## Libretto

Luigi Dallapiccola

## Fonti del libretto

Villiers de l’Isle-Adam, *La torture par l’espèrance*
e Charles de Coster, *La lègende d’Ulenspiegel et de Lamme Goedzak*

## Trama

Epoca: la seconda metà del XVI secolo. Il prigioniero è detenuto nelle
carceri spagnole, al tempo del re Filippo II. Riceve la visita della
madre, perseguitata da un incubo, raccontato nel prologo, in cui il re le
si presenta nelle vesti della Morte. Il prigioniero ricorda che dopo le
torture qualcuno lo ha chiamato fratello, e sembra avere un momento di
sollievo.

Entra il carceriere, che nuovamente usa la parola fratello,
e gli annuncia che la rivolta degli accattoni ha avuto successo. Nel
prigioniero rinasce la speranza, e il sentimento si rinforza quando scopre
che il carceriere è uscito lasciando aperta la porta del carcere. Il
prigioniero tenta la fuga, nei corridoi riesce ad evitare due sacerdoti
che discorrono tra loro, poi esce in un giardino.

Qui viene catturato dal grande Inquisitore, che ha la stessa figura e la stessa voce del
carceriere, che lo chiama ancora una volta fratello ma poi dolcemente
lo conduce al rogo. "La libertà?", si chiede il prigioniero sussurrando
quasi incosciente, dopo avere guardato il rogo ridendo come un pazzo.[4]
